# -*- coding: utf-8 -*-
"""
Created on Tue May 31 20:54:23 2022

@author: dinho
"""

import matplotlib.pyplot as plt
import numpy as np
import random as rd
import math

taxa_mutacao = 0.3#0.6
taxa_cruzamento = 0.7
cromossomo = 3
tam_pop = 50
geracao = 0
geracao_max = 100
rodadas = 5
sigma = 0.05#0.001
torneio = math.floor(tam_pop * 0.5) # define qual a porcentagem da população que irá pro torneio
taxa_decressimo = 0.90  #a cada 10 gerações o valor é decrementado em 10%
count = 0
best_pop_qtd = 10
delta = 0.01
total_parada = math.floor(geracao_max * 0.2)
metade_pop = int(round(tam_pop/2))
lb = np.array([-3, -2, 0]) #valores minimos
ub = np.array([3, 4, 6]) #valores máximos
best = []

geracao_muda_porcent = 10
count_muda = 0

mean = [] #armazena a média
std = [] #armazena o desvio padrão

pop_children = [] # vetor de armazenamento temporário dos filhos AG

fitness_best_el = [] #vetor de armazenamento permanente dos filhos RW
fitness_best_ag = [] #vetor de armazenamento permanente dos filhos AG

""" Especifica o domínio da função -> caracteriza o espaço de busca """
delta_I = 0
delta_F = 1

def generate_population_real(tam_pop=tam_pop): #cria a população e normaliza
    population = np.zeros((tam_pop, cromossomo))
    cromosso = np.zeros((tam_pop, cromossomo))
    
        # populacao sequencial
    for i in range(tam_pop): # preenche a matriz com valores entre 0 e 1
        for j in range(cromossomo):
            population[i,j] = np.random.uniform(0,1)
        
        # normalização
    for i in range(tam_pop): #formula de conversão dos valores x1, x2, x3
        for j in range(cromossomo):
            cromosso[i][j] = lb[j] + (ub[j] - lb[j]) * population[i][j]
            
    return cromosso

def fitness_function(cromosso): #função de fitness
    ap = np.zeros(len(cromosso))
    
    for i in range(len(cromosso)):
        ap[i] = (10*(cromosso[i][0] - 1)**2) + (20*(cromosso[i][1] - 2)**2) + (30*(cromosso[i][2] - 3)**2)
        
    return ap

def tournament_selection(pop, k, fitness): #função da seleção por torneio
    N = len(pop)
    
    best = 0
    index_best = 0
    
    choices = np.arange(0, N, dtype=int) # cria uma lista de todos os index dos indivíduos
    
    index_selected = np.random.choice(choices, size=k, replace=False) # seleciona uma quantidade k de indivíduos que iram pro torneio
    
    for i in range(len(index_selected)): # seleciona o individuo com o melhor fitness
        if i == 0:
            best = fitness[index_selected[i]]
            index_best = index_selected[i]
        
        elif fitness[index_selected[i]] < best:
            best = fitness[index_selected[i]]
            index_best = index_selected[i]
            
    return index_best

def media_arithmetic_crossover(parent_1, parent_2): #método de cruzamento 
    children = []
    
    if(rd.random() < taxa_cruzamento):
        new_1 = np.zeros(cromossomo)

        for i in range(cromossomo):
            new_1[i] = ((parent_1[i] + parent_2[i]) / 2)

        children = np.append(children, new_1)
    else:
        children = np.append(children, parent_1)
    
    return  np.reshape(children, (1,cromossomo))

def gaussian_mutation(children): #método de mutação
    children2 = np.zeros((1, cromossomo))
    
    for i in range(len(children[0])):
        
        if(rd.random() < taxa_mutacao):
            value_sorted = np.random.randn(1, 1)
            children2[0][i] = (sigma * value_sorted[0][0]) + children[0][i]
        else:
            children2[0][i] = children[0][i]
            
    return children2

def select_best_fitness(fitness): # método de seleção dos 50% melhores fitness (RW)
    melhor_aptidao = np.zeros(metade_pop)
    fitness_asc = np.sort(fitness)
    
    for i in range(metade_pop):
        melhor_aptidao[i] = fitness_asc[i]
        
    return melhor_aptidao

def select_worst_indexes(pop, fitness): # método de seleção dos 50% piores indivíduos (RW)
    pior_aptidao = np.zeros(metade_pop, dtype=int)
    pior = np.sort(fitness)
    melhorAptidaoDesc = np.flip(pior)
    
    for i in range(metade_pop):
        pior = np.where(fitness == (melhorAptidaoDesc[i]))
        pior_aptidao[i] = int(pior[0])
        
    return pior_aptidao

def generate_worst_pop(pop, worst): # Gera uma população adicionando novos indivíduos no lugar dos 50% piores
    new_pop = np.copy(pop)

    for i in range(len(worst)):
        for j in range(cromossomo):
            new_pop[worst[i], j] = np.random.uniform(0,1)

    
    for i in range(len(worst)):
        for j in range(cromossomo):
            new_pop[worst[i], j] = lb[j] + (ub[j] - lb[j]) * new_pop[worst[i], j]
            
    return new_pop

def stop_the_count(melhor_aptidao_1, melhor_aptidao_2, count):
    evo_fitness = abs((melhor_aptidao_2 - melhor_aptidao_1)/melhor_aptidao_1)
    #print(evo_fitness)
    
    if(evo_fitness < delta):
        #print(count)
        count = count + 1
    else:
        #print('nova contagem')
        count = 0
        
    return count

def graph (geracao, fitness, j, type_ag):
    x = []
    y = []
    y1 = []
    y2 = []
    
    for i in range(geracao):
        x.append(i)
        y.append(np.mean(fitness[i]))
        y1.append(np.mean(fitness[i]) - np.std(fitness[i]))
        y2.append(np.mean(fitness[i]) + np.std(fitness[i]))
    
    fig, ax = plt.subplots()
    
    fig.figsize=(1000,1000)
    fig.dpi=100
    ax.plot(x,y,'-')
    plt.xlabel('gerações')
    plt.ylabel('fitness')
    plt.title("rodada " + str(j) + "\n" + type_ag + " com " + str(geracao) + " gerações e " + str(tam_pop) + " de população")
        
    ax.fill_between(x,y2,y1,alpha=0.5)

    plt.gca()
    plt.show()

for rodada in range(rodadas):
    population_ag = generate_population_real(tam_pop) # gera uma população já convertida
    population_el = population_ag # copia a população inicial para ser usada no RW
    
    while(geracao < geracao_max): #rodada do AG
    
        value_fitness = fitness_function(population_ag) #gera o fitness dos indivíduos da população
        best_fitness_old = np.sort(value_fitness)[0]
        
        #controle para se ter o tamanho da população de filhos igual a dos pais
        while(int(len(pop_children)/cromossomo) != tam_pop):
            index_parent_1 = tournament_selection(population_ag, torneio, value_fitness)
                                                                                            #
                                                                                            #
            for j in range(10000):                                                          #
                index_parent_2 = tournament_selection(population_ag, torneio, value_fitness)   # faz a seleção dos pais que iram pro cruzamento
                                                                                            #
                if(index_parent_2 != index_parent_1):                                       #
                    break
            children = media_arithmetic_crossover(population_ag[index_parent_1], population_ag[index_parent_2])
            children = gaussian_mutation(children) # armazena os valores da mutação
    
            pop_children = np.append(pop_children, children)  

        pop_children = np.reshape(pop_children, (tam_pop, cromossomo)) # reorganiza o vetor para o formato da população inicial
    
        fitness_children_ag = np.sort(fitness_function(pop_children)) # fitness dos filhos                                                          #
        """
        if(len(fitness_children_ag) != best_pop_qtd):
            fitness_children = fitness_children_ag
            fitness_children_ag_asc = np.sort(fitness_children)
            
            for i in range (best_pop_qtd):
                best.append(fitness_children_ag_asc[i])
            
            fitness_best_ag.append(best)
            best = []
        else:
            fitness_best_ag.append(fitness_children_ag) # fitness dos filhos
        """
        fitness_best_ag.append(fitness_children_ag) # fitness dos filhos
        population_ag = pop_children # os filhos assumem o lugar dos pais
        pop_children = [] # reseta o vetor de filhos
        
        qtd_contagem = stop_the_count(best_fitness_old, fitness_children_ag[0], count)
        count = qtd_contagem
        #print(count)
        
        if(count_muda == geracao_muda_porcent): # a cada 10% do total de gerações, a mutação e o sigma é diminuido
            taxa_mutacao = taxa_mutacao * taxa_decressimo
            sigma = sigma * taxa_decressimo
        else:
            count_muda = count_muda + 1
        
        if(qtd_contagem == total_parada):
            break
        else:
            geracao = geracao + 1
            
    graph(geracao, fitness_best_ag, (rodada + 1), "AG")
    fitness_best_ag = []
    geracao = 0
    count = 0
    count_muda = 0
    taxa_mutacao = 0.3
    sigma = 0.05
    
    while(geracao < geracao_max): #rodada do RW
        fitness_el = fitness_function(population_el) # fitness do RW
        
        fitness_best_old = np.sort(fitness_el)[0]
        
        melhores_fitness_1 = select_best_fitness(fitness_el) # seleciona os 50% melhores fitness
        piores_indices1 = select_worst_indexes(population_el, fitness_el) # seleciona os 50% piores indivíduos
        
        pop_children_el = generate_worst_pop(population_el, piores_indices1) # nova população já substituindo os piores
        fitness_children_el = np.sort(fitness_function(pop_children_el)) # fitness dos filhos
        
        if(len(fitness_children_el) != best_pop_qtd):
            fitness_children = fitness_children_el
            fitness_children_el_asc = np.sort(fitness_children)
                
            for i in range (best_pop_qtd):
                best.append(fitness_children_el_asc[i])
                
            fitness_best_el.append(best)
            best = []
        else:
            fitness_best_el.append(fitness_children_el) # fitness dos filhos
        
        #fitness_best_el.append(fitness_children_el)
        population_el = pop_children_el
        
        qtd_contagem = stop_the_count(best_fitness_old, fitness_children_el[0], count)
        count = qtd_contagem
        
        if(qtd_contagem == total_parada):
            break
        else:
            geracao = geracao + 1

    graph(geracao, fitness_best_el, (rodada + 1), "RW")
    fitness_best_el = []
    geracao = 0
    count = 0

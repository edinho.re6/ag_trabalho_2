# -*- coding: utf-8 -*-
"""
Created on Wed Jun  1 23:13:56 2022

@author: dinho
"""

import matplotlib.pyplot as plt
import numpy as np
import random as rd
import math

taxa_mutacao = 0.1#0.6
taxa_cruzamento = 0.6
geracao_max = 100
sigma = 0.001
taxa_decressimo = 0.01  #a cada 10 gerações o valor é decrementado em 1%
count = 0

geracao_muda_porcent = geracao_max*0.1

pop_palavra = np.array([52.547, 72.154, 53.694, 57.771, 115.88,
                105.59, 75.368, 126.02, 52.756, 85.100,
                80.525, 111.24, 113.62, 64.95,  89.181,
                85.647, 101.71, 106.75, 110.37, 72.082,
                104.38, 102.41, 63.009, 59.52, 89.869,
                126.78, 77.231, 96.821, 67.905, 110.1
                ])

tam_pop = len(pop_palavra)
torneio = math.floor(tam_pop * 0.5) # define qual a porcentagem da população que irá pro torneio

mean = [] #armazena a média
std = [] #armazena o desvio padrão

pop_children = [] # vetor de armazenamento temporário dos filhos
fitness_children_ag = [] #vetor permanente

""" Especifica o domínio da função -> caracteriza o espaço de busca """
delta_I = 0
delta_F = 1

lb = np.min(pop_palavra) #valores minimos
ub = np.max(pop_palavra) #valores máximos

metade_pop = int(round(tam_pop/2))
fitness_best = [] #vetor de armazenamento permanente dos filhos RW

def generate_population_real(tam_pop=tam_pop): #cria a população e normaliza
    population = np.zeros((tam_pop))
    cromosso = np.zeros((tam_pop))
    
        # populacao sequencial
    for i in range(tam_pop): # preenche a matriz com valores entre 0 e 1
            population[i] = np.random.uniform(0,1)
        
        # normalização
    for i in range(tam_pop): #formula de conversão dos valores x1, x2, x3
            cromosso[i] = lb + (ub - lb) * population[i]
            
    return cromosso

def fitness_function(cromosso, palavra): #função de fitness
    ap = np.zeros(len(cromosso))
    
    for i in range(len(cromosso)):
        ap[i] = abs(palavra[i] - cromosso[i])
        
    return ap

def tournament_selection(pop, k, fitness): #função da seleção por torneio
    N = len(pop)
    
    best = 0
    index_best = 0
    
    choices = np.arange(0, N, dtype=int) # cria uma lista de todos os index dos indivíduos
    
    index_selected = np.random.choice(choices, size=k, replace=False) # seleciona uma quantidade k de indivíduos que iram pro torneio
    
    for i in range(len(index_selected)): # seleciona o individuo com o melhor fitness
        if i == 0:
            best = fitness[index_selected[i]]
            index_best = index_selected[i]
        
        elif fitness[index_selected[i]] < best:
            best = fitness[index_selected[i]]
            index_best = index_selected[i]
            
    return index_best

def media_arithmetic_crossover(parent_1, parent_2): #método de cruzamento 
    children = []
    
    if(rd.random() < taxa_cruzamento):
        new_1 = 0

        new_1 = ((parent_1 + parent_2) / 2)

        children = np.append(children, new_1)
    else:
        children = np.append(children, parent_1)
    
    return  children

def gaussian_mutation(children): #método de mutação
    children2 = np.zeros(1)
        
    if(rd.random() < taxa_mutacao):
        value_sorted = np.random.randn(1, 1)
         
        children2[0] = (sigma * value_sorted[0][0]) + children[0]
    else:
        children2[0] = children[0]
            
    return children2

def select_best_fitness(fitness): # método de seleção dos 50% melhores fitness (RW)
    melhor_aptidao = np.zeros(metade_pop)
    fitness_asc = np.sort(fitness)
    #print(fitness_asc)
    #print("------------------------------------------------------")
    
    for i in range(metade_pop):
        melhor_aptidao[i] = fitness_asc[i]
        
    return melhor_aptidao

def select_worst_indexes(pop, fitness): # método de seleção dos 50% piores indivíduos (RW)
    pior_aptidao = np.zeros(metade_pop, dtype=int)
    pior = np.sort(fitness)
    melhorAptidaoDesc = np.flip(pior)
    
    for i in range(metade_pop):
        pior = np.where(fitness == (melhorAptidaoDesc[i]))
        pior_aptidao[i] = int(pior[0])
        
    return pior_aptidao

def generate_worst_pop(pop, worst): # Gera uma população adicionando novos indivíduos no lugar dos 50% piores
    new_pop = np.copy(pop)
    
    for i in range(len(worst)):
        new_pop[worst[i]] = np.random.uniform(0,1)
        
    for i in range(len(worst)):
        new_pop[worst[i]] = lb + (ub - lb) * new_pop[worst[i]]
            
    
    return new_pop

population_ag = generate_population_real(tam_pop)# gera uma população já convertida
population_el = population_ag # copia a população inicial para ser usada no RW

for i in range(geracao_max):
    
    value_fitness = fitness_function(population_ag, pop_palavra) #gera o fitness dos indivíduos da população
    
    #controle para se ter o tamanho da população de filhos igual a dos pais
    while(int(len(pop_children)) != tam_pop):
        
        index_parent_1 = tournament_selection(population_ag, torneio, value_fitness)  
                                                                                        #
                                                                                        #
        for j in range(10000):                                                          #
            index_parent_2 = tournament_selection(population_ag, torneio, value_fitness)   # faz a seleção dos pais que iram pro cruzamento
                                                                                        #
            if(index_parent_2 != index_parent_1):                                       #
                break                                                                   #
        
        children = media_arithmetic_crossover(population_ag[index_parent_1], population_ag[index_parent_2])
        children = gaussian_mutation(children) # armazena os valores da mutação
    
        pop_children = np.append(pop_children, children)
    
    fitness_children_ag.append(fitness_function(population_ag, pop_palavra)) # fitness dos filhos
    
    population_ag = pop_children  # os filhos assumem o lugar dos pais
    
    pop_children = [] # reseta o vetor de filhos
    
    #ELITISMO ------------------------------------------------------------------
    fitness_el = fitness_function(population_el, pop_palavra) # fitness do RW
    
    melhores_fitness_1 = select_best_fitness(fitness_el) # seleciona os 50% melhores fitness
    piores_indices1 = select_worst_indexes(population_el, fitness_el) # seleciona os 50% piores indivíduos
    
    pop_children_el = generate_worst_pop(population_el, piores_indices1) # nova população já substituindo os piores
    
    fitness_children_el = fitness_function(pop_children_el, pop_palavra) # fitness dos filhos
    
    melhores_fitness_2 = select_best_fitness(fitness_children_el) # seleciona os 50% melhores fitness dos filhos
    
    fitness_best.append(np.append(melhores_fitness_1, melhores_fitness_2)) # junta os 50% melhores dos pais e dos filhos
    
    population_el = pop_children_el
    """
    count = count + 1
    if(count < geracao_muda_porcent):
        sigma = sigma * taxa_decressimo
        taxa_cruzamento = taxa_cruzamento * taxa_decressimo
        taxa_mutacao = taxa_mutacao * taxa_decressimo
        count = 0
    """

x = []
y = []
y1 = []
y2 = []

for i in range(geracao_max):
            
    x.append(i)
    y.append(np.mean(fitness_children_ag[i]))
    y1.append(np.mean(fitness_children_ag[i]) - np.std(fitness_children_ag[i]))
    y2.append(np.mean(fitness_children_ag[i]) + np.std(fitness_children_ag[i]))
        
fig, ax = plt.subplots()
    
fig.figsize=(1000,1000)
fig.dpi=100
ax.plot(x,y,'-')
plt.xlabel('gerações')
plt.ylabel('fitness')
plt.title("AG com " + str(geracao_max) + " gerações")
    
ax.fill_between(x,y2,y1,alpha=0.5)

plt.gca()
plt.show()

x = []
y = []
y1 = []
y2 = []

for i in range(geracao_max):
            
    x.append(i)
    y.append(np.mean(fitness_best[i]))
    y1.append(np.mean(fitness_best[i]) - np.std(fitness_best[i]))
    y2.append(np.mean(fitness_best[i]) + np.std(fitness_best[i]))
        
fig, ax = plt.subplots()
    
fig.figsize=(1000,1000)
fig.dpi=100
ax.plot(x,y,'-')
plt.xlabel('gerações')
plt.ylabel('fitness')
plt.title("AG Random Walk com " + str(geracao_max) + " gerações")    
ax.fill_between(x,y2,y1,alpha=0.5)
plt.gca()
plt.show()

print("Palavra dada:")
print(pop_palavra)
print("---------------------------------------------")

print("Palavra obtida AG:")
print(population_ag)
print("---------------------------------------------")

print("Palavra obtida AG RW:")
print(pop_children_el)
print("---------------------------------------------")